﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Vaisseau_Control : MonoBehaviour {
    private Rigidbody2D rBvaisseau;
    private PolygonCollider2D bodyCollider;
    public float rotationSpeed;
    public GameObject projectile;
    public float projectileSpeed;

	// Use this for initialization
	void Start ()
    {
        rBvaisseau = GetComponent<Rigidbody2D>();
        bodyCollider = GetComponent<PolygonCollider2D>();

        Vector2 direction = new Vector2(Random.Range(-1.0f, 1.0f), Random.Range(-1.0f, 1.0f)).normalized;

	}
	
	// Update is called once per frame
	void Update ()
    {
        Walk();
        Look();
        Shoot();
	}
    private void Walk()
    {
        if (Input.GetKeyDown(KeyCode.UpArrow))
        {
            rBvaisseau.velocity = new Vector2(Mathf.Clamp(rBvaisseau.velocity.x + this.transform.right.x , 0, 10),rBvaisseau.velocity.y + this.transform.right.y);
        }
        if (Input.GetKeyDown(KeyCode.DownArrow))
        {
            rBvaisseau.velocity = new Vector2(rBvaisseau.velocity.x - this.transform.right.x, rBvaisseau.velocity.y - this.transform.right.y);
        }
    }

    private void Look()
    {
        LeftArrow();
        RightArrow();
        
    }
    private void LeftArrow()
    {
        if (Input.GetKey(KeyCode.LeftArrow))
        {
            this.transform.Rotate(Vector3.forward, rotationSpeed);
        }
    }    
    private void RightArrow()
    {
        if (Input.GetKey(KeyCode.RightArrow))
        {
            this.transform.Rotate(Vector3.forward, -rotationSpeed);
        }
    }
    private void Shoot ()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            GameObject newProjectile = Instantiate(projectile.gameObject, this.transform.position, Quaternion.identity);
            newProjectile.GetComponent<Rigidbody2D>().velocity = this.transform.right * projectileSpeed;

        }
    }
}

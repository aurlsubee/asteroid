﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TourelleAttack : MonoBehaviour {

    private CircleCollider2D tourelle;
    public GameObject projectile;
    public float speedProjectile;

	// Use this for initialization
	void Start ()
    {
        tourelle = GetComponent<CircleCollider2D>();
	}

    private void OnTriggerEnter2D(Collider2D other)
    {
        if(other.gameObject.CompareTag("Asteroid"))
        {
            Debug.Log("Asteroid detecté");
            GameObject newProjectile = Instantiate(projectile.gameObject, this.transform.position, Quaternion.identity);
            newProjectile.GetComponent<Rigidbody2D>().velocity = other.transform.position * speedProjectile;
        }
    }
}

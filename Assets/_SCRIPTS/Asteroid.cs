﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Asteroid : MonoBehaviour
{
    //Je declare mes variables.
	public float speed; //je declare un float.
    private Vector2 randDirection; // je declare un vecteur2.
    private Rigidbody2D refRigidbody; // je declare un rigidBody.
    public int asteroidLevel = 2;
    public int nbAsteroid = 2;

    //demarage du script.
	void Start () 
	{
		refRigidbody = this.gameObject.GetComponent<Rigidbody2D> ();                            // J'assigne un rigidBody a ma variable qui peut contenir un rigidBody.
        randDirection = new Vector2(Random.Range(-1f,1f), Random.Range(-1f,1f)).normalized;     // j'assigne deux valeur random au x et y de mon vecteur2.
        refRigidbody.velocity = randDirection * speed;                                          //j'assigne une vitesse a mon rigidbody.
	}

    private void OnTriggerEnter2D(Collider2D other)
    {
        if(other.CompareTag("Projectile") || other.CompareTag("Player"))
        {
            if(asteroidLevel > 0 && other.CompareTag("Projectile"))
            {
                for (int i = 0; i < nbAsteroid; i++) 
                {
                    //Creation du nouvel Ateroid 
                    GameObject newAsteroid = Instantiate(this.gameObject, this.transform.position, Quaternion.identity);
                    //Modification de la taille du nouvel Asteroid
                    newAsteroid.transform.localScale = this.transform.localScale * 0.5f;
                    //On recupere la position du script du nouveau asteroid
                    Asteroid newAsteroidScript = newAsteroid.GetComponent<Asteroid>();

                    //le nouveau asteroid est de niveau inferieur et se divisera en plus grand nombre que le precedent 
                    newAsteroidScript.asteroidLevel--;

                    
                }
                //Destruction du projectile et de l'asteroid touché. 
                Destroy(other.gameObject);
                Destroy(this.gameObject);

            }
                Destroy(other.gameObject);
        }
       

    }
}

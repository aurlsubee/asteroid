﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MyGameManager : MonoBehaviour {

    //je declare mes variables.
    private Camera cam;                                                                                                     //Je declare une camera.
    private Vector3 screenWorldPoint;                                                                                       //je declare un vector3.
    private BoxCollider2D collision;                                                                                        //je declare un BoxCollider2D.
    private Vector2 position;                                                                                               //je declare un vecteur2.

    // Use this for initialization
    void Start()
    {
        AllGetComponent();
        GetScreenWorldPoint();
        SetCollider();
    }

    private void OnTriggerExit2D(Collider2D other)                                                                          //Je trouve une collision avec un objet.
    {
        WarpObject(other);  
    }
    private void WarpObject(Collider2D other)
    {
        if (other.transform.position.x > screenWorldPoint.x || other.transform.position.x < -screenWorldPoint.x)            // je verifie si l'objet a traverssé a gauche ou a droite.
        {
            position = other.transform.position;                                                                            //j'assigne la position de l'objet a mon vecteur2.
            other.transform.position = new Vector3(-position.x, position.y, 0);                                              //j'envoi l'objet a la position opposé.
        }         //je verifie la position x de l'objet qui traverse
        if (other.transform.position.y > screenWorldPoint.y || other.transform.position.y < -screenWorldPoint.y)            // je verifie si l'objet a traverssé en haut ou en bas
        {
            position = other.transform.position;                                                                            //j'assigne la position de l'objet a mon vecteur2.
            other.transform.position = new Vector3(position.x, -position.y, 0);                                             //j'envoi l'objet a la position opposé.
        }         //je verifie la position y de l'objet qui traverse

    }
    private void AllGetComponent()
    {
        cam = GetComponent<Camera>();                                                                                       //je met une camera dans cam.
        collision = GetComponent<BoxCollider2D>();                                                                          //je met une BoxCollider2D dans ma variable collision.
    }
    private void GetScreenWorldPoint()
    {
        screenWorldPoint = cam.ScreenToWorldPoint(new Vector3(Screen.width, Screen.height, 0.0f));                          //je converti les valeur pixel de l'ecran en vecteur3
    }
    private void SetCollider()
    {
        collision.size = new Vector2(screenWorldPoint.x * 2.0f, screenWorldPoint.y * 2.0f);                                 //je fait corespondre ma boite de collision a ma camera.
    }

}
